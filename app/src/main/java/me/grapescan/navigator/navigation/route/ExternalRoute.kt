package me.grapescan.navigator.navigation.route

import me.grapescan.navigator.BuildConfig

/**
 * Defines external routes.
 */
sealed class ExternalRoute(url: String) : Route.External(url) {
    /**
     * Defines the route to get a view navigated to a FAQ page.
     */
    class FAQ(url: String) : ExternalRoute(url)

    /**
     * Defines the route to get a view navigated to app page in Google Play.
     */
    object GooglePlay : ExternalRoute("https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}")
}
