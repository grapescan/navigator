package me.grapescan.navigator.navigation.delegate

import me.grapescan.navigator.view.BaseDialogFragment

/**
 * The [DialogFragmentNavigationDelegate] is a sub class of the [ActivityNavigationDelegate] to navigate within a fragment context.
 *
 * @param fragment the [BaseDialogFragment]
 */
class DialogFragmentNavigationDelegate constructor(private val fragment: BaseDialogFragment) :
    ActivityNavigationDelegate(fragment.obtainActivity()) {

    override fun navigateBack() = fragment.popBackStack()
}
