package me.grapescan.navigator.sample

import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import me.grapescan.navigator.R
import me.grapescan.navigator.navigation.route.AppRoute
import me.grapescan.navigator.view.BaseActivity

class MainActivity : BaseActivity() {

    private val navigator by lazy { App.injector.navigator }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        sample1.setOnClickListener { navigator.navigateTo(AppRoute.Details) }
    }
}
