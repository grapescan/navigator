package me.grapescan.navigator.navigation

import me.grapescan.navigator.navigation.delegate.NavigationDelegate
import me.grapescan.navigator.navigation.route.Route

/**
 * The [Navigator] is a base implementation of the [NavigationDelegate].
 */
class Navigator : NavigationDelegate {

    private val delegates = mutableListOf<NavigationDelegate>()

    override fun navigateTo(route: Route, replace: Boolean): Boolean {
        delegates.forEach {
            if (it.navigateTo(route, replace)) return true
        }
        return false
    }

    /**
     * Delegate performing navigation by some other [NavigationDelegate] instance.
     *
     * **NOTE** It may result to the memory leaks, DO NOT forget to call delegate(NavigationDelegate, false)}
     *
     * @param delegate the [NavigationDelegate]
     * @param enable   `true` if it is needed to be delegated by the passed delegate, `false` if it is needed to remove delegation by the passed delegate
     */
    fun delegate(delegate: NavigationDelegate, enable: Boolean) {
        if (enable) {
            delegates.add(delegate)
        } else {
            delegates.remove(delegate)
        }
    }
}
