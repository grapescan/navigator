package me.grapescan.navigator.navigation.delegate

import android.content.Intent
import me.grapescan.navigator.view.BaseActivity

/**
 * The [ActivityNavigationDelegate] is a sub class of the [ApplicationNavigationDelegate] to navigate within an activity context.
 *
 * @param context the [BaseActivity]
 */
open class ActivityNavigationDelegate constructor(context: BaseActivity) :
    ApplicationNavigationDelegate<BaseActivity>(context) {

    override fun showActivity(intent: Intent, replace: Boolean, anim: Pair<Int, Int>?): Boolean {
        context.startActivity(intent)
        if (replace) {
            context.supportFinishAfterTransition()
        }
        anim?.run { context.overridePendingTransition(first, second) }

        return true
    }

    override fun navigateBack() = context.popBackStack()

    /**
     * Restart a current screen,
     */
    override fun restartScreen() = context.recreate().let { true }
}
