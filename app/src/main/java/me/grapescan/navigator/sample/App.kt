package me.grapescan.navigator.sample

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import me.grapescan.navigator.navigation.Navigator
import me.grapescan.navigator.navigation.delegate.ActivityNavigationDelegate
import me.grapescan.navigator.navigation.delegate.DialogFragmentNavigationDelegate
import me.grapescan.navigator.navigation.delegate.FragmentNavigationDelegate
import me.grapescan.navigator.navigation.delegate.NavigationDelegate
import me.grapescan.navigator.view.BaseActivity
import me.grapescan.navigator.view.BaseDialogFragment
import me.grapescan.navigator.view.BaseFragment

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        context = this
    }

    companion object {

        @SuppressLint("StaticFieldLeak")
        private var context: Context? = null

        val injector by lazy { Injector(context!!) }

    }
}

class Injector(context: Context) {

    fun dialogFragmentNavigationDelegate(dialogFragment: BaseDialogFragment): NavigationDelegate =
        DialogFragmentNavigationDelegate(dialogFragment)

    fun fragmentNavigationDelegate(fragment: BaseFragment): NavigationDelegate = FragmentNavigationDelegate(fragment)
    fun activityNavigationDelegate(activity: BaseActivity): NavigationDelegate = ActivityNavigationDelegate(activity)
    val navigator: Navigator = Navigator()
}
