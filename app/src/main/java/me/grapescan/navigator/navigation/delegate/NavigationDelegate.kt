package me.grapescan.navigator.navigation.delegate

import me.grapescan.navigator.navigation.route.Route

/**
 * The [NavigationDelegate] is responsible for navigation between views within all the application.
 *
 * **NOTE:** the term View defines whatever that can represent some information
 * whether it is a text or picture or sound and so on,
 * everything that can be noted by the end user.
 */
interface NavigationDelegate {
    /**
     * Navigate to the given route.
     *
     * @param route the route to navigate
     * @param replace defines whether navigating to the new route should replace the current one
     *
     * @return `true` if the rout was handled, `false` otherwise
     */
    fun navigateTo(route: Route, replace: Boolean = false): Boolean
}
