package me.grapescan.navigator.view

import me.grapescan.navigator.navigation.route.Route

/**
 * Able to handle navigation to another view according to the [Route].
 */
interface NavigableView {
    /**
     * Navigate to another view in the navigation stack.
     * A default implementation calls corresponding [execute] method with the [Navigation] action.
     *
     * @param route the address to navigate, by default it requires to navigate to the previous view
     * @param replace defines whether navigating to the new route should replace the current one
     */
    fun navigateTo(route: Route = Route.Back, replace: Boolean = false)
}
