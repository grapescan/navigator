package me.grapescan.navigator.view

import androidx.fragment.app.Fragment
import me.grapescan.navigator.navigation.route.Route
import me.grapescan.navigator.sample.App

/**
 * The [BaseFragment] defines the base behavior for all the fragments within the app.
 * NOTE: It should be used together with [BaseActivity].
 */
abstract class BaseFragment : Fragment(), NavigableView {

    private val navigationDelegate by lazy { App.injector.fragmentNavigationDelegate(this) }
    private val navigator by lazy { App.injector.navigator }

    /**
     * Check to see whether this fragment is in the process of finishing.
     *
     * @return Return `true` if the fragment is currently being to destroyed/finished, otherwise `false`
     */
    val isFinishing: Boolean
        get() = activity?.isFinishing ?: false || isRemoving

    override fun onResume() {
        super.onResume()
        navigator.delegate(navigationDelegate, true)
    }

    override fun onPause() {
        super.onPause()
        navigator.delegate(navigationDelegate, false)
    }

    /**
     * Return an [BaseActivity].
     */
    fun obtainActivity() = activity as BaseActivity

    /**
     * Pops a child fragment from a stack.
     *
     * @return 'true' if a back navigation is handled, 'false' otherwise
     */
    open fun popBackStack(): Boolean =
        !childFragmentManager.isStateSaved && childFragmentManager.popBackStackImmediate()

    override fun navigateTo(route: Route, replace: Boolean) {
        navigator.navigateTo(route, replace)
    }
}
