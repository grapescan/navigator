package me.grapescan.navigator.view

import android.app.Dialog
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatDialog
import androidx.fragment.app.DialogFragment
import me.grapescan.navigator.navigation.route.Route
import me.grapescan.navigator.sample.App

/**
 * The [BaseDialogFragment] defines the base behavior for all the dialog fragments within the app.
 * It contains some workaround to fix the known issues of the [DialogFragment][android.support.v4.app.DialogFragment].
 * It contains some workaround to use an [AppCompatDialog] in place of a platform-styled dialog.
 *
 * NOTE: It should be used together with [BaseActivity].
 *
 * @see android.support.v4.app.DialogFragment
 * @see android.support.v7.app.AppCompatDialogFragment
 */
abstract class BaseDialogFragment : DialogFragment(), NavigableView {

    init {
        retainInstance = true
        arguments = Bundle()
    }

    private val navigationDelegate by lazy { App.injector.dialogFragmentNavigationDelegate(this) }
    private val navigator by lazy { App.injector.navigator }

    /**
     * Check to see whether this fragment is in the process of finishing.
     *
     * @return Return `true` if the fragment is currently being to destroyed/finished, otherwise `false`
     */
    val isFinishing: Boolean
        get() = activity?.isFinishing ?: false || isRemoving

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog = AppCompatDialog(context, theme)

    override fun setupDialog(dialog: Dialog, style: Int) {
        if (dialog is AppCompatDialog) {
            // If the dialog is an AppCompatDialog, we'll handle it
            when (style) {
                DialogFragment.STYLE_NO_INPUT -> {
                    dialog.getWindow()!!.addFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                    dialog.supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
                }
                DialogFragment.STYLE_NO_FRAME,
                DialogFragment.STYLE_NO_TITLE -> dialog.supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
                else -> super.setupDialog(dialog, style)
            }
        } else {
            // Else, just let super handle it
            super.setupDialog(dialog, style)
        }
    }

    override fun onResume() {
        super.onResume()
        navigator.delegate(navigationDelegate, true)
    }

    override fun onPause() {
        super.onPause()
        navigator.delegate(navigationDelegate, false)
    }

    override fun onDestroyView() {
        //known issue with dismissing dialog after change device configuration https://code.google.com/p/android/issues/detail?id=17423
        if (dialog != null && retainInstance) {
            dialog!!.setOnDismissListener(null)
        }
        super.onDestroyView()
    }

    /**
     * Return an [BaseActivity].
     */
    fun obtainActivity() = activity as BaseActivity

    /**
     * Pops a child fragment from a stack.
     *
     * @return 'true' if a back navigation is handled, 'false' otherwise
     */
    open fun popBackStack(): Boolean =
        !childFragmentManager.isStateSaved && childFragmentManager.popBackStackImmediate()

    override fun navigateTo(route: Route, replace: Boolean) {
        navigator.navigateTo(route, replace)
    }
}
