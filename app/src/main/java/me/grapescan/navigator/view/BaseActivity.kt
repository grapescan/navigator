package me.grapescan.navigator.view

import android.view.MenuItem
import androidx.fragment.app.FragmentActivity
import me.grapescan.navigator.navigation.route.Route
import me.grapescan.navigator.sample.App

/**
 * The [BaseActivity] defines the base behavior of all the activities within the app.
 */
abstract class BaseActivity : FragmentActivity(), NavigableView {

    private val navigator by lazy { App.injector.navigator }
    private val navigationDelegate by lazy { App.injector.activityNavigationDelegate(this) }

    override fun onResume() {
        super.onResume()
        navigator.delegate(navigationDelegate, true)
    }

    override fun onOptionsItemSelected(item: MenuItem) =
        if (android.R.id.home == item.itemId) navigator.navigateTo(Route.Back) else super.onOptionsItemSelected(item)

    override fun onBackPressed() {
        navigator.navigateTo(Route.Back)
    }

    override fun onPause() {
        super.onPause()
        navigator.delegate(navigationDelegate, false)
    }

    /**
     * Pops a fragment from a stack, or finish this activity if the fragment stack is empty.
     *
     * @return 'true' if a back navigation is handled, 'false' otherwise
     */
    open fun popBackStack() = !isFinishing && supportFragmentManager.run {
        if (isStateSaved || !popBackStackImmediate()) {
            finishAfterTransition()
        }
        true
    }

    override fun navigateTo(route: Route, replace: Boolean) {
        navigator.navigateTo(route, replace)
    }
}
