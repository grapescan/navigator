package me.grapescan.navigator.navigation.delegate

import android.content.Context
import android.content.Intent
import me.grapescan.navigator.navigation.route.AppRoute
import me.grapescan.navigator.navigation.route.Route
import me.grapescan.navigator.sample.DetailsActivity

open class ApplicationNavigationDelegate<T : Context>(protected val context: T) : NavigationDelegate {

    override fun navigateTo(route: Route, replace: Boolean) = when (route) {
        Route.Back -> navigateBack()
        Route.Restart -> restartScreen()
        AppRoute.Details -> showActivity(DetailsActivity.createIntent(context), replace)
        is AppRoute.StackRoute -> stackTo(route, replace)
        else -> false
    }

    private fun stackTo(stack: AppRoute.StackRoute, replace: Boolean): Boolean {
        return navigateTo(AppRoute.Home, replace) && navigateTo(stack.route, replace)
    }

    /**
     * Navigate back.
     *
     * @return 'true' if a back navigation is handled, 'false' otherwise
     */
    protected open fun navigateBack(): Boolean = false

    /**
     * Restart a current screen.
     *
     * @return 'true' if restarting is handled, 'false' otherwise
     */
    protected open fun restartScreen(): Boolean = false

    /**
     * Show an activity by the passed [Intent]
     *
     * @param intent the [Intent]
     * @param replace defines whether the current one activity should be closed
     * @param anim the [Pair] that contains a first value as an enter animation and the second as an exit anim
     */
    protected open fun showActivity(intent: Intent, replace: Boolean, anim: Pair<Int, Int>? = null): Boolean {
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        context.startActivity(intent)
        return true
    }
}
