package me.grapescan.navigator.navigation.route

import java.io.Serializable

/**
 * Defines an application navigation route.
 */
sealed class Route(val id: String) {
    /**
     * Defines a route to navigate back in the app or close it if it is no possible.
     */
    object Back : Route("navigation://app/go_back")

    /**
     *  Defines a route to get a view restarted.
     */
    object Restart : Route("navigation://app/restart")

    /**
     *  Defines a base internal application route.
     *
     *  @param path the sub path of the route
     */
    open class Inner(path: String) : Route("navigation://app/$path")

    /**
     *  Defines a base external application route.
     *
     *  *NOTE* it should not be used often just really special cases.
     *
     *  @param path defines an external path.
     *  @param code defines a unique request code
     *  @param data defines an extra data
     */
    open class External(path: String, val code: Int = 0, val data: Map<String, Serializable> = emptyMap()) :
        Route(path) {
        init {
            if (path.isBlank()) {
                throw IllegalArgumentException("The path can not be an empty")
            }
        }
    }

    /**
     * Defines a route to show a notification.
     *
     * @param notificationMessage the [NotificationMessage] to be handled
     */
    open class Notification(val notificationMessage: String) : Route("navigation://app/notification")

    override fun hashCode() = id.hashCode()

    override fun equals(other: Any?) = this === other || other is Route && id == other.id
}
