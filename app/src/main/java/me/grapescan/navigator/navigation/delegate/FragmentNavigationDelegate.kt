package me.grapescan.navigator.navigation.delegate

import me.grapescan.navigator.view.BaseFragment

/**
 * The [FragmentNavigationDelegate] is a sub class of the [ActivityNavigationDelegate] to navigate within a fragment context.
 *
 * @param fragment the [BaseFragment]
 */
class FragmentNavigationDelegate constructor(private val fragment: BaseFragment) :
    ActivityNavigationDelegate(fragment.obtainActivity()) {

    override fun navigateBack() = fragment.popBackStack()
}
