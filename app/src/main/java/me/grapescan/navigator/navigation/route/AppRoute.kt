package me.grapescan.navigator.navigation.route

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Defines inner application routes.
 */
@Parcelize
open class AppRoute(val path: String) : Route.Inner(path), Parcelable {

    object Home : AppRoute("home")

    object Details : AppRoute("details")

    data class StackRoute(val route: AppRoute) : AppRoute("stack/${route.id}")
}
